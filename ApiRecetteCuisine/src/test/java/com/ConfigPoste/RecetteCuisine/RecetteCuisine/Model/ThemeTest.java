package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThemeTest {

    private Theme theme;

    @BeforeEach
    void setUp() {
        theme=new Theme();
        theme.setId(1);
        theme.setNom("dessert");
    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void getId() {
        int id=theme.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        theme.setId(id);
        assertEquals(id,theme.getId());
    }

    @Test
    void getNom() {
        String nom=theme.getNom();
        assertEquals(nom,"dessert");
    }

    @Test
    void setNom() {
        String nomNew="plat";
        theme.setNom(nomNew);
        assertEquals(nomNew,theme.getNom());
    }
}