package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Controller;

import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.Unite;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.UniteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UniteController.class)
class UniteControllerTest {



    private Unite unite;
    private String listJson;
    private Collection<Unite> unites;
    @Autowired
    protected MockMvc mvc;
    @MockBean
    private UniteRepository uniteRepository;

    @BeforeEach
    void setUp() {
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        unites=new ArrayList<>();
        unites.add(unite);
        listJson=asJsonString(unites);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getList() throws Exception {
        String uri="http://localhost:9000/Unites";
        when(uniteRepository.findAll()).thenReturn(unites);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,listJson);
    }

    @Test
    void afficherUnite() throws Exception {
        String uri="http://localhost:9000/Unites/1";
        when(uniteRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(unite));
        String etpaeString=asJsonString(unite);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,etpaeString);
    }

    @Test
    void saveUnite() throws Exception {
        String uri="http://localhost:9000/Unites/create";
        Unite prod=new Unite();
        prod.setId(2);
        prod.setValue("ajout farine");
        String prodString=asJsonString(prod);
        when(uniteRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}