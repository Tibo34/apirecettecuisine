package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.criteria.CriteriaBuilder;

import static org.junit.jupiter.api.Assertions.*;

class IngredientTest {

    private Ingredient ingredient;
    private TypeIngredient type;
    private Unite unite;

    @BeforeEach
    void setUp() {
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        type=new TypeIngredient();
        type.setId(1);
        type.setImage("oeuf.png");
        type.setNom("oeuf");
        type.setUnite(unite);
        ingredient=new Ingredient();
        ingredient.setId(1);
        ingredient.setTypeId(1);
        ingredient.setId_image(1);
        ingredient.setRecetteId(1);
        ingredient.setType(type);
        ingredient.setQuantite(20.0);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {
        int id=ingredient.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        ingredient.setId(id);
        assertEquals(id,ingredient.getId());
    }

    @Test
    void getTypeId() {
        int idtype=ingredient.getTypeId();
        assertEquals(idtype,1);
    }

    @Test
    void setTypeId() {
        int idtype=2;
        ingredient.setTypeId(idtype);
        assertEquals(ingredient.getTypeId(),idtype);
    }

    @Test
    void getQuantite() {
        double quantite=ingredient.getQuantite();
        assertEquals(quantite,20.0);
    }

    @Test
    void setQuantite() {
        double quantite=30.0;
        ingredient.setQuantite(quantite);
        assertEquals(quantite,ingredient.getQuantite());
    }

    @Test
    void getRecetteId() {
        int id=ingredient.getRecetteId();
        assertEquals(id,1);
    }

    @Test
    void setRecetteId() {
        int id=2;
        ingredient.setRecetteId(id);
        assertEquals(id,ingredient.getRecetteId());
    }

    @Test
    void getType() {
        TypeIngredient typeGet=ingredient.getType();
        assertEquals(typeGet,type);
    }

    @Test
    void setType() {
        TypeIngredient typeNew=new TypeIngredient();
        typeNew.setId(2);
        ingredient.setType(typeNew);
        assertEquals(typeNew,ingredient.getType());
    }

    @Test
    void getId_image() {
        int idImage=ingredient.getId_image();
        assertEquals(idImage,1);
    }

    @Test
    void setId_image() {
        int idImage=2;
        ingredient.setId_image(idImage);
        assertEquals(ingredient.getId_image(),idImage);
    }
}