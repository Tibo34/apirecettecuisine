package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.assertj.core.util.diff.Delta;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TypeIngredientTest {

    private TypeIngredient type;
    private Unite unite;
    @BeforeEach
    void setUp() {
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        type=new TypeIngredient();
        type.setId(1);
        type.setImage("oeuf.png");
        type.setNom("oeuf");
        type.setUnite(unite);
        type.setImageId("1");
        type.setUniteId(1);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getImage() {
        String img=type.getImage();
        assertEquals(img,"oeuf.png");
    }

    @Test
    void setImage() {
        String newImg="farine.png";
        type.setImage(newImg);
        assertEquals(newImg,type.getImage());
    }

    @Test
    void getId() {
        int id=type.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        type.setId(id);
        assertEquals(id,type.getId());
    }

    @Test
    void getNom() {
        String nom=type.getNom();
        assertEquals(nom,"oeuf");
    }

    @Test
    void setNom() {
        String nomNew="farine";
        type.setNom(nomNew);
        assertEquals(nomNew,type.getNom());
    }

    @Test
    void getUniteId() {
        int uniteid=type.getUniteId();
        assertEquals(uniteid,1);
    }

    @Test
    void setUniteId() {
        int id=2;
        type.setUniteId(id);
        assertEquals(id,type.getUniteId());
    }

    @Test
    void testEquals() {
        Unite uniteNew=new Unite();
        uniteNew.setId(1);
        uniteNew.setValue("g");
        TypeIngredient typeNew=new TypeIngredient();
        typeNew.setId(1);
        typeNew.setImage("oeuf.png");
        typeNew.setNom("oeuf");
        typeNew.setUnite(unite);
        typeNew.setImageId("1");
        typeNew.setUniteId(1);
        assertEquals(typeNew,type);
    }

    @Test
    void testHashCode() {
        Unite uniteNew=new Unite();
        uniteNew.setId(1);
        uniteNew.setValue("g");
        TypeIngredient typeNew=new TypeIngredient();
        typeNew.setId(1);
        typeNew.setImage("oeuf.png");
        typeNew.setNom("oeuf");
        typeNew.setUnite(unite);
        typeNew.setImageId("1");
        typeNew.setUniteId(1);
        assertEquals(typeNew,type);
    }

    @Test
    void getUnite() {
        Unite uniteGet=type.getUnite();
        assertEquals(uniteGet,unite);
    }

    @Test
    void setUnite() {
        Unite uniteNew=new Unite();
        uniteNew.setId(2);
        uniteNew.setValue("L");
        type.setUnite(uniteNew);
        assertEquals(uniteNew,type.getUnite());
    }

    @Test
    void getImageId() {
        String idImage=type.getImageId();
        assertEquals(idImage,"1");
    }

    @Test
    void setImageId() {
        String idImage="2";
        type.setImageId(idImage);
        assertEquals(idImage,type.getImageId());
    }
}