package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Controller;

import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.Etape;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.EtapeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EtapeController.class)
class EtapeControllerTest {

    private Collection<Etape> etapes;
    private Etape etape;
    private String listJson;

    @Autowired
    protected MockMvc mvc;
    @MockBean
    private EtapeRepository etapeRepository;

    @BeforeEach
    void setUp() {
        etape=new Etape();
        etape.setRang(1);
        etape.setId(1);
        etape.setRecette_id(1);
        etape.setContent("ajout oeuf");
        etapes=new ArrayList<>();
        etapes.add(etape);
        listJson=asJsonString(etapes);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getList() throws Exception {
        String uri="http://localhost:9000/Etapes";
        when(etapeRepository.findAll()).thenReturn(etapes);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,listJson);
    }

    @Test
    void afficherEtape() throws Exception {
        String uri="http://localhost:9000/Etapes/1";
        when(etapeRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(etape));
        String etpaeString=asJsonString(etape);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,etpaeString);
    }

    @Test
    void createIngredient() throws Exception {
        String uri="http://localhost:9000/Etapes/create";
        Etape prod=new Etape();
        prod.setId(2);
        prod.setContent("ajout farine");
        String prodString=asJsonString(prod);
        when(etapeRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateIngredient() throws Exception {
        String uri="http://localhost:9000/Etapes/update";
        Etape prod=new Etape();
        prod.setId(1);
        prod.setContent("ajout farine");
        String prodString=asJsonString(prod);
        when(etapeRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



}