package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class RecetteTest {

    private Ingredient ingredient;
    private Collection<Ingredient> ingredients;
    private TypeIngredient type;
    private Unite unite;
    private Recette recette;
    private Etape etape;
    private Collection<Etape> etapes;
    private Theme theme;

    @BeforeEach
    void setUp() {
        theme=new Theme();
        theme.setId(1);
        theme.setNom("dessert");
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        type=new TypeIngredient();
        type.setId(1);
        type.setImage("oeuf.png");
        type.setNom("oeuf");
        type.setUnite(unite);
        ingredient=new Ingredient();
        ingredient.setId(1);
        ingredient.setTypeId(1);
        ingredient.setId_image(1);
        ingredient.setRecetteId(1);
        ingredient.setType(type);
        ingredient.setQuantite(20.0);
        ingredients=new ArrayList<>();
        ingredients.add(ingredient);
        etape=new Etape();
        etape.setRang(1);
        etape.setId(1);
        etape.setRecette_id(1);
        etape.setContent("ajout oeuf");
        etapes=new ArrayList<>();
        etapes.add(etape);
        recette=new Recette();
        recette.setId(1);
        recette.setImage("tarte chocolat");
        recette.setImageId("1");
        recette.setNom("tarte aux chocolat");
        recette.setPersonneMin(4);
        recette.setTempCuisson(20);
        recette.setTempPreparation(20);
        recette.setTheme(theme);
        recette.setShortContent("tarte");
        recette.setEtapes(etapes);
        recette.setIngredients(ingredients);
    }

    @AfterEach
    void tearDown() {
    }



    @Test
    void getPersonneMin() {
        int min=recette.getPersonneMin();
        assertEquals(min,recette.getPersonneMin());
    }

    @Test
    void setPersonneMin() {
        int min=5;
        recette.setPersonneMin(min);
        assertEquals(min,recette.getPersonneMin());
    }

    @Test
    void getTempCuisson() {
        int cuisson=recette.getTempCuisson();
        assertEquals(cuisson,20);
    }

    @Test
    void setTempCuisson() {
        int cuisson=30;
        recette.setTempCuisson(cuisson);
        assertEquals(cuisson,recette.getTempCuisson());
    }

    @Test
    void getTempPreparation() {
        int preparation=recette.getTempPreparation();
        assertEquals(preparation,20);
    }

    @Test
    void setTempPreparation() {
        int preparation=30;
        recette.setTempPreparation(preparation);
        assertEquals(preparation,recette.getTempPreparation());
    }

    @Test
    void getEtapes() {
        Collection<Etape> list=recette.getEtapes();
        assertEquals(list.size(),1);
    }

    @Test
    void setEtapes() {
        Etape etape1=new Etape();
        etape1.setId(2);
        etapes.add(etape1);
        recette.setEtapes(etapes);
        assertEquals(recette.getEtapes().size(),2);
    }

    @Test
    void getId() {
        int id=recette.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        recette.setId(id);
        assertEquals(id,recette.getId());
    }

    @Test
    void getNom() {
        String nom=recette.getNom();
        assertEquals(nom,"tarte aux chocolat");
    }

    @Test
    void setNom() {
        String nom="tart au citron";
        recette.setNom(nom);
        assertEquals(nom,recette.getNom());
    }

    @Test
    void testEquals() {
        Recette recetteNew=new Recette();
        recetteNew.setId(1);
        recetteNew.setImage("tarte chocolat");
        recetteNew.setImageId("1");
        recetteNew.setNom("tarte aux chocolat");
        recetteNew.setPersonneMin(4);
        recetteNew.setTempCuisson(20);
        recetteNew.setTempPreparation(20);
        recetteNew.setTheme(theme);
        recetteNew.setShortContent("tarte");
        recetteNew.setEtapes(etapes);
        recetteNew.setIngredients(ingredients);
        assertEquals(recetteNew,recette);
    }

    @Test
    void testHashCode() {
        Recette recetteNew=new Recette();
        recetteNew.setId(1);
        recetteNew.setImage("tarte chocolat");
        recetteNew.setImageId("1");
        recetteNew.setNom("tarte aux chocolat");
        recetteNew.setPersonneMin(4);
        recetteNew.setTempCuisson(20);
        recetteNew.setTempPreparation(20);
        recetteNew.setTheme(theme);
        recetteNew.setShortContent("tarte");
        recetteNew.setEtapes(etapes);
        recetteNew.setIngredients(ingredients);
        assertEquals(recetteNew,recette);
    }

    @Test
    void getShortContent() {
        String content=recette.getShortContent();
        assertEquals(content,"tarte");
    }

    @Test
    void setShortContent() {
        String content="salade";
        recette.setShortContent(content);
        assertEquals(content,recette.getShortContent());
    }

    @Test
    void getTheme() {
        Theme themeNew=recette.getTheme();
        assertEquals(themeNew,theme);
    }

    @Test
    void setTheme() {
        Theme themeN=new Theme();
        themeN.setId(2);
        themeN.setNom("dessert");
        recette.setTheme(themeN);
        assertEquals(themeN,recette.getTheme());
    }

    @Test
    void getIngredients() {
        Collection<Ingredient> ingredientsN=recette.getIngredients();
        assertEquals(ingredientsN.size(),1);
    }

    @Test
    void setIngredients() {
        Ingredient ingredient1=new Ingredient();
        ingredient1.setId(2);
        ingredient1.setTypeId(1);
        ingredient1.setId_image(1);
        ingredient1.setRecetteId(1);
        ingredient1.setType(type);
        ingredient1.setQuantite(20.0);
        ingredients.add(ingredient1);
        recette.setIngredients(ingredients);
        assertEquals(recette.getIngredients().size(),2);
    }

    @Test
    void getImage() {
        String img=recette.getImage();
        assertEquals(img,"tarte chocolat");
    }

    @Test
    void setImage() {
        String img="flan caramel";
        recette.setImage(img);
        assertEquals(img,recette.getImage());
    }

    @Test
    void getImageId() {
        String img=recette.getImageId();
        assertEquals(img,"1");
    }

    @Test
    void setImageId() {
        String idImg="2";
        recette.setImageId(idImg);
        assertEquals(idImg,recette.getImageId());
    }
}