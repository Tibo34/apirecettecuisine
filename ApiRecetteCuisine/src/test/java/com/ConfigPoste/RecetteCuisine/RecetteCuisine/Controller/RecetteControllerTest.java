package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Controller;

import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.*;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.IngredientRepository;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.RecetteRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RecetteController.class)
class RecetteControllerTest {

    private Ingredient ingredient;
    private Collection<Ingredient> ingredients;
    private TypeIngredient type;
    private Unite unite;
    private Recette recette;
    private Etape etape;
    private Collection<Etape> etapes;
    private Theme theme;
    private Collection<Recette> recettes;
    private String listJson;
    @Autowired
    protected MockMvc mvc;
    @MockBean
    private RecetteRepository recetteRepository;


    @BeforeEach
    void setUp() {
        theme=new Theme();
        theme.setId(1);
        theme.setNom("dessert");
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        type=new TypeIngredient();
        type.setId(1);
        type.setImage("oeuf.png");
        type.setNom("oeuf");
        type.setUnite(unite);
        ingredient=new Ingredient();
        ingredient.setId(1);
        ingredient.setTypeId(1);
        ingredient.setId_image(1);
        ingredient.setRecetteId(1);
        ingredient.setType(type);
        ingredient.setQuantite(20.0);
        ingredients=new ArrayList<>();
        ingredients.add(ingredient);
        etape=new Etape();
        etape.setRang(1);
        etape.setId(1);
        etape.setRecette_id(1);
        etape.setContent("ajout oeuf");
        etapes=new ArrayList<>();
        etapes.add(etape);
        recette=new Recette();
        recette.setId(1);
        recette.setImage("tarte chocolat");
        recette.setImageId("1");
        recette.setNom("tarte aux chocolat");
        recette.setPersonneMin(4);
        recette.setTempCuisson(20);
        recette.setTempPreparation(20);
        recette.setTheme(theme);
        recette.setShortContent("tarte");
        recette.setEtapes(etapes);
        recette.setIngredients(ingredients);
        recettes=new ArrayList<>();
        recettes.add(recette);
        listJson=asJsonString(recettes);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getList() throws Exception {
        String uri="http://localhost:9000/Recettes";
        when(recetteRepository.findAll()).thenReturn(recettes);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,listJson);
    }

    @Test
    void afficherRecette() throws Exception {
        String uri="http://localhost:9000/Recettes/1";
        when(recetteRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(recette));
        String etpaeString=asJsonString(recette);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,etpaeString);
    }

    @Test
    void saveRecette() throws Exception {
        String uri="http://localhost:9000/Recettes/create";
        Recette prod=new Recette();
        prod.setId(2);
        String prodString=asJsonString(prod);
        when(recetteRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }




    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}