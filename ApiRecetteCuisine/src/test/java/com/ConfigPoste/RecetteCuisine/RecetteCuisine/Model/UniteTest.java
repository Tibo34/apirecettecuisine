package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniteTest {

    private Unite unite;

    @BeforeEach
    void setUp() {
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {
        int id=unite.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        unite.setId(2);
        assertEquals(unite.getId(),2);
    }

    @Test
    void getValue() {
        String value=unite.getValue();
        assertEquals(value,"g");
    }

    @Test
    void setValue() {
        String value="L";
        unite.setValue(value);
        assertEquals(value,unite.getValue());
    }

    @Test
    void testEquals() {
        Unite uniteNew=new Unite();
        uniteNew.setId(1);
        uniteNew.setValue("g");
        assertEquals(uniteNew,unite);
    }

    @Test
    void testHashCode() {
        Unite uniteNew=new Unite();
        uniteNew.setId(1);
        uniteNew.setValue("g");
        assertEquals(uniteNew,unite);
    }
}