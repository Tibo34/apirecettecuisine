package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Controller;

import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.Theme;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.ThemeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ThemeController.class)
class ThemeControllerTest {

    private Theme theme;
    private Collection<Theme> themes;

    private String listJson;

    @Autowired
    protected MockMvc mvc;
    @MockBean
    private ThemeRepository themeRepository;

    @BeforeEach
    void setUp() {
        theme=new Theme();
        theme.setId(1);
        theme.setNom("dessert");
        themes =new ArrayList<>();
        themes.add(theme);
        listJson=asJsonString(themes);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getList() throws Exception {
        String uri="http://localhost:9000/Themes";
        when(themeRepository.findAll()).thenReturn(themes);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,listJson);
    }

    @Test
    void afficherEtape() throws Exception {
        String uri="http://localhost:9000/Themes/1";
        when(themeRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(theme));
        String etpaeString=asJsonString(theme);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,etpaeString);
    }

    @Test
    void createIngredient() throws Exception {
        String uri="http://localhost:9000/Themes/create";
        Theme prod=new Theme();
        prod.setId(2);
        prod.setNom("ajout farine");
        String prodString=asJsonString(prod);
        when(themeRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateIngredient() throws Exception {
        String uri="http://localhost:9000/Themes/update";
        Theme prod=new Theme();
        prod.setId(1);
        prod.setNom("ajout farine");
        String prodString=asJsonString(prod);
        when(themeRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}