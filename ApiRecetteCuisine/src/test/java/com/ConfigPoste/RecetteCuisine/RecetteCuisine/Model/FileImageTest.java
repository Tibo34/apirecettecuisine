package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileImageTest {


    private  FileImage file;
    private String hello="Hello world";
    private byte[] dataHello=hello.getBytes();
    @BeforeEach
    void setUp() {
        file=new FileImage();
        file.setId(1);
        file.setFileName("citron.jpg");
        file.setFileType("image");
        byte[] datas=hello.getBytes();
        file.setData(datas);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {
        int id=file.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        file.setId(id);
        assertEquals(id,file.getId());
    }

    @Test
    void getFileName() {
        String filename=file.getFileName();
        assertEquals(filename,"citron.jpg");
    }

    @Test
    void setFileName() {
        String fileNameNew="farine.jpg";
        file.setFileName(fileNameNew);
        assertEquals(fileNameNew,file.getFileName());
    }

    @Test
    void getData() {
        byte[] datas=file.getData();
        String newHello=new String(datas);
        assertEquals(newHello,hello);
    }

    @Test
    void setData() {
        byte[] newData="Coucou test spring".getBytes();
        file.setData(newData);
        assertEquals(newData,file.getData());
    }

    @Test
    void getFileType() {
        String mime=file.getFileType();
        assertEquals(mime,"image");
    }

    @Test
    void setFileType() {
        String mime="pdf";
        file.setFileType(mime);
        assertEquals(mime,file.getFileType());
    }
}