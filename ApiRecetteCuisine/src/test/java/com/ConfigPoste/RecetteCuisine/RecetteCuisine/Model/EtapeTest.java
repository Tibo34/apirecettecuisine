package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EtapeTest {

    private Etape etape;

    @BeforeEach
    void setUp() {
        etape=new Etape();
        etape.setRang(1);
        etape.setId(1);
        etape.setRecette_id(1);
        etape.setContent("ajout oeuf");
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getRang() {
        int rang=etape.getRang();
        assertEquals(rang,1);
    }

    @Test
    void setRang() {
        int rang=2;
        etape.setRang(rang);
        assertEquals(rang,etape.getRang());
    }

    @Test
    void getRecette_id() {
        int recettId=etape.getRecette_id();
        assertEquals(recettId,1);
    }

    @Test
    void setRecette_id() {
        int id=2;
        etape.setRecette_id(id);
        assertEquals(id,etape.getRecette_id());
    }

    @Test
    void getId() {
        int id=etape.getId();
        assertEquals(id,1);
    }

    @Test
    void setId() {
        int id=2;
        etape.setId(id);
        assertEquals(id,etape.getId());
    }

    @Test
    void getContent() {
        String content=etape.getContent();
        assertEquals(content,"ajout oeuf");
    }

    @Test
    void setContent() {
        String content="ajout farine";
        etape.setContent(content);
        assertEquals(content,etape.getContent());
    }
}