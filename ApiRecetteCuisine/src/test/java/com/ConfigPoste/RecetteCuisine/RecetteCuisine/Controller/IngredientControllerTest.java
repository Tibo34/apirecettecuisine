package com.ConfigPoste.RecetteCuisine.RecetteCuisine.Controller;

import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.Ingredient;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.TypeIngredient;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Model.Unite;
import com.ConfigPoste.RecetteCuisine.RecetteCuisine.Repository.IngredientRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(IngredientController.class)
class IngredientControllerTest {

    private Ingredient ingredient;
    private TypeIngredient type;
    private Unite unite;
    private String listJson;
    private Collection<Ingredient> ingredients;
    @Autowired
    protected MockMvc mvc;
    @MockBean
    private IngredientRepository ingredientRepository;

    @BeforeEach
    void setUp() {
        unite=new Unite();
        unite.setId(1);
        unite.setValue("g");
        type=new TypeIngredient();
        type.setId(1);
        type.setImage("oeuf.png");
        type.setNom("oeuf");
        type.setUnite(unite);
        ingredient=new Ingredient();
        ingredient.setId(1);
        ingredient.setTypeId(1);
        ingredient.setId_image(1);
        ingredient.setRecetteId(1);
        ingredient.setType(type);
        ingredient.setQuantite(20.0);
        ingredients=new ArrayList<>();
        ingredients.add(ingredient);
        listJson=asJsonString(ingredients);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getList() throws Exception {
        String uri="http://localhost:9000/Ingredients";
        when(ingredientRepository.findAll()).thenReturn(ingredients);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,listJson);
    }

    @Test
    void afficherIngredient() throws Exception {
        String uri="http://localhost:9000/Ingredients/1";
        when(ingredientRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(ingredient));
        String etpaeString=asJsonString(ingredient);
        ResultActions perform = mvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_UTF8_VALUE));
        perform.andExpect(status().isOk());
        String contentAsString = perform.andReturn().getResponse().getContentAsString();
        assertEquals(contentAsString,etpaeString);
    }

    @Test
    void createIngredient() throws Exception {
        String uri="http://localhost:9000/Ingredients/create";
        Ingredient prod=new Ingredient();
        prod.setId(2);
        String prodString=asJsonString(prod);
        when(ingredientRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }

    @Test
    void updateIngredient() throws Exception {
        String uri="http://localhost:9000/Ingredients/update";
        Ingredient prod=new Ingredient();
        prod.setId(1);
        prod.setId_image(2);
        String prodString=asJsonString(prod);
        when(ingredientRepository.save(prod)).thenReturn(prod);
        ResultActions resultActions = mvc.perform(post(uri)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(prodString)
        ).andDo(print()).andExpect(status().isOk());
    }




    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}